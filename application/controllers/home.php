	<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$this->load->model('Home_model');
		$this->load->model('Mysql_model');
		$this->load->helper(array('form','url','file','download','text'));
		$this->load->helper('menu_helper');
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));

	}
	public function index(){
		$this->load->model('Pasca_model');
		$tahun =  Date("Y");
		$data['getAllMhsBaru'] = $this->Home_model->getAllMhsBaru($tahun);
		$data['getAllMhs'] = $this->Home_model->getAllMhs();
		$data['getAllMhsAsing'] = $this->Home_model->getAllMhsAsing();
		$data['getAllMhsPasca'] = $this->Pasca_model->getAllMhsPasca();
		print_r ($data['getAllMhsPasca']);
		$this->load->view('header');
		$this->load->view('form_akademik',$data);
	}
	public function mhs(){
		$data['getAllMhs'] = $this->Home_model->getAllMhs();

		/* $data['getAllMhs'] = $this->Home_model->getAllMhs();
		$data['getAllMhs'] = $this->Home_model->getAllMhsFak();
		$data['getAllStaff'] = $this->Mysql_model->getAllStaff();
		$data['getAllDosen'] = $this->Mysql_model->getAllDosen();
		 *///$data['hak'] = $this->Mysql_model->coba2();
		//$this->load->view('grafik/grafik',$data);

	}

	public function sdm(){

		$data['getAllStaff'] = $this->Mysql_model->getAllStaff();
		$data['getAllDosen'] = $this->Mysql_model->getAllDosen();
		$data['getAllGuruBesar'] = $this->Mysql_model->getAllGuruBesar();
		$data['getAllTotalLulusanS1'] = $this->Mysql_model->getAllTotalLulusanS1();
		$data['getDosenLulusanS1'] = $this->Mysql_model->getDosenLulusanS1();
		$data['getPegawaiLulusanS1'] = $this->Mysql_model->getPegawaiLulusanS1();
		$data['getAllTotalLulusanS2'] = $this->Mysql_model->getAllTotalLulusanS2();
		$data['getDosenLulusanS2'] = $this->Mysql_model->getDosenLulusanS2();
		$data['getPegawaiLulusanS2'] = $this->Mysql_model->getPegawaiLulusanS2();
		$data['getAllTotalLulusanS3'] = $this->Mysql_model->getAllTotalLulusanS3();
		$data['getDosenLulusanS3'] = $this->Mysql_model->getDosenLulusanS3();
		$data['getPegawaiLulusanS3'] = $this->Mysql_model->getPegawaiLulusanS3();
		$data['getPangkatDosen'] = $this->Mysql_model->getPangkatDosen();
		$data['getPangkatPegawai'] = $this->Mysql_model->getPangkatPegawai();
		$data['getJafungDosen'] = $this->Mysql_model->getJafungDosen();
		// print_r ($data['getDosenLulusanS1']);
		$this->load->view('header');
		$this->load->view('form_sdm',$data);
	}
	public function kemahasiswaan(){
		$data['getAllMhsFakultas'] = $this->Home_model->getAllMhsFakultas();
		$data['getAllMhsJK'] = $this->Home_model->getAllMhsJK();
		$tahun =  Date("Y");
		$data['getAllMhsPerTahun'] = $this->Home_model->getAllMhsPerTahun($tahun);
		// print_r ($data['getAllMhsPerTahun']);
		$this->load->view('header');
		$this->load->view('form_kemahasiswaan',$data);
	}

}
