	<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Edit extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->helper(array('form','url','file','download','text'));
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));

	}
	public function index(){
		$data['getAllMhs'] = $this->Home_model->getAllMhs();
		$data['getAllStaff'] = $this->Mysql_model->getAllStaff();
		$data['getAllDosen'] = $this->Mysql_model->getAllDosen();
		//$data['hak'] = $this->Mysql_model->coba2();
		$this->load->view('grafik/grafik',$data);
	}
	public function akademik(){
		$this->load->view('header');
		$this->load->view('form_akademik');
	}
	public function sdm(){
		$this->load->view('header');
		$this->load->view('form_sdm');
	}
	public function kemahasiswaan(){
		$this->load->view('header');
		$this->load->view('form_kemahasiswaan');
	}

}
