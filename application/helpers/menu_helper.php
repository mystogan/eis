<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('active_link')){
	function activate_menu($controller){
		//getting ci class instance
		$CI = get_instance();
		//getting router class to active_link
		$class = $CI->router->fetch_class();
		return ($class == $controller)? 'active':"";
	}
}?>