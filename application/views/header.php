<html>
  <head><meta charset="utf-8">
    <link href='<?php echo base_url();?>assets/images/logo2.png' rel='SHORTCUT ICON'/>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Executive Information System</title>

	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/themify-icons.css">
	<!-- DataTables CSS -->
	<link href="<?php echo base_url();?>assets/css/dataTables.bootstrap.css" rel="stylesheet">

	<!-- DataTables Responsive CSS -->
	<link href="<?php echo base_url();?>assets/css/dataTables.responsive.css" rel="stylesheet">


  	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/reset.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-table.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/headerfooter_akademik.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/form_akademik.css" type="text/css "/>


	<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">

	<script>
  base_url = "<?php echo base_url();?>";
	$(document).ready(function() {
		$('#dataTables-example').DataTable({
				responsive: true
		});
	});
	</script>
  </head>
<style>
.nav li a:hover{
	background-color:#f0ad4e;
}
</style>


<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

<nav class="navbar navbar-default navbar-fixed-top" style="border-bottom:3px solid #f0ad4e;">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#myPage">Executive Information System</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="<?php echo base_url();?>home"><span class="glyphicon glyphicon-education"></span> AKADEMIK</a></li>
        <li><a href="<?php echo base_url();?>home/sdm"><span class="glyphicon glyphicon-user"></span> SDM</a></li>
        <li><a href="<?php echo base_url();?>home/kemahasiswaan"><span class="glyphicon glyphicon-hourglass"></span> KEMAHASISWAAN</a></li>
      </ul>
    </div>
  </div>
	<!--<div class="tambahnav" style="background-color:#f0ad4e;height:3em;">

	</div>-->
</nav>



    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.dataTables.min.js" type="text/javascript" ></script>
	<script src="<?php echo base_url();?>assets/js/dataTables.bootstrap.min.js" type="text/javascript" ></script>
	<script src="<?php echo base_url();?>assets/js/dataTables.responsive.js" type="text/javascript" ></script>
	<!-- CKeditor CSS -->
	<script src="<?php echo base_url();?>assets/js/ckeditor.js"></script>


<script>
$(document).ready(function(){
  // Add smooth scrolling to all links in navbar + footer link
  $(".navbar a, footer a[href='#myPage']").on('click', function(event) {
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 900, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });

   $(".slideanim").each(function(){
      var pos = $(this).offset().top;

      var winTop = $(window).scrollTop();
        if (pos < winTop + 600) {
          $(this).addClass("slide");
        }
    });
  $(window).scroll(function() {
  $(".slideanim").each(function(){
      var pos = $(this).offset().top;

      var winTop = $(window).scrollTop();
        if (pos < winTop + 600) {
          $(this).addClass("slide");
        }
    });
  });

})
</script>
