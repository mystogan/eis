<html>
  <head><meta charset="utf-8">
    <link href='<?php echo base_url();?>assets/images/logo.png' rel='SHORTCUT ICON'/>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Executive Information System</title>
  	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/reset.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/form.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-table.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/headerfooter.css" type="text/css "/>

	<!-- DataTables CSS -->
	<link href="<?php echo base_url();?>assets/css/dataTables.bootstrap.css" rel="stylesheet">

	<!-- DataTables Responsive CSS -->
	<link href="<?php echo base_url();?>assets/css/dataTables.responsive.css" rel="stylesheet">

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.dataTables.min.js" type="text/javascript" ></script>
	<script src="<?php echo base_url();?>assets/js/dataTables.bootstrap.min.js" type="text/javascript" ></script>
	<script src="<?php echo base_url();?>assets/js/dataTables.responsive.js" type="text/javascript" ></script>
	<!-- CKeditor CSS -->
	<script src="<?php echo base_url();?>assets/js/ckeditor.js"></script>
	<script src="<?php echo base_url();?>assets/js/proses.js"></script>
	
	
	<script src="<?php echo base_url();?>assets/js/grafik/snap.svg-min.js"></script>
	
	<!-- highcharts-->
	<script src="<?php echo base_url();?>assets/js/grafik/proses.js"></script>
	
	
	<script>
  base_url = "<?php echo base_url();?>";
	$(document).ready(function() {
		$('#dataTables-example').DataTable({
				responsive: true
		});
	});
	</script>
  </head>

<style>
  .menu{
	  padding:.3em;
	  text-align:center;
	  padding-top:1.2em;
	  padding-bottom:1.2em;
	  width:100%;
  }
</style>

<body>
<div class="row">
	<div class="container">	
		<div class="col-sm-offset-11 col-md-offset-12">
			<div class="side_bar pull-right">
				<nav id="sidebar-wrapper">
				  <ul class="sidebar-nav">
					<li class="sidebar-nav-item" style="padding:.9em;margin-top:8em">
					  <img width="60em" src="<?php echo base_url();?>assets/images/uin.png"/>
					</li>
					<li class="sidebar-nav-item">
					  <a class="js-scroll-trigger btn btn-default menu" href="#page-top"><span class="glyphicon glyphicon-home"></span></br>Beranda</a>
					</li>
					<li class="sidebar-nav-item">
					  <a class="js-scroll-trigger btn btn-default menu" href="#about"><span class="glyphicon glyphicon-education"></span></br>Akademik</a>
					</li>
					<li class="sidebar-nav-item">
					  <a class="js-scroll-trigger btn btn-default menu" href="#services">Services</a>
					</li>
					<li class="sidebar-nav-item">
					  <a class="js-scroll-trigger btn btn-default menu" href="#portfolio">Portfolio</a>
					</li>
					<li class="sidebar-nav-item">
					  <a class="js-scroll-trigger btn btn-default menu" href="#contact">Contact</a>
					</li>
				  </ul>
				</nav>

			</div>
		</div>
	</div>
</div>


<div class="container" >
	<div class="row" >
		<div class="col-md-3 jud">
			<img width="75em" src="<?php echo base_url();?>assets/images/logo2.png"/>
		</div>
		<div class="col-md-8 judul">
			Executive Information System</br>
			<h4>UIN Sunan Ampel Surabaya</h4>
		</div>
	</div>
</div>













