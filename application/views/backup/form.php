<style>
.panelAwal{
	margin-top:3em;
}
</style>


	<section class="content-section bg-light" id="about">
      <div class="container " >
        <div class="row">
          <div class="col-sm-10 col-lg-11 mx-auto" >
			<hr width="100%">
			<h3 style="color:#fff;text-align:right;font-weight:bold">AKADEMIK</h3>
			<hr width="100%">
			<div class="row">
				<div class="col-xs-12 col-md-7">
					<div class="panel panel-default ">
						<div class="panel-body">
						<p style="font-size:1.3em">Jumlah Mahasiswa Aktif</p><hr>
							<div class="row">
							<div class="col-md-6">
							  <ul data-pie-id="svg" style="text-align:right;">
								<li data-value="60">Adab dan Humaniora (60)</li>
								<li data-value="20">Dakwah dan Komunikasi (20)</li>
								<li data-value="12">Tarbiyah dan Keguruan (12)</li>
								<li data-value="32">Ushuludin dan Filsafat (32)</li>
								<li data-value="50">Syariah dan Hukum (50)</li>
								<li data-value="50">Sains an Teknologi (50)</li>
								<li data-value="50">Ekonomi dan Bisnis Islam (50)</li>
								<li data-value="50">Psikologi dan Kesehatan (30)</li>
								<li data-value="50">Ilmu Sosial dan Ilmu Politik (50)</li>
							  </ul>
							</div>
							<div class="col-md-6">
							  <div id="svg"></div>
							</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-5">
					<div class="panel panel-default ">
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12 col-md-3">
								  <span style="font-size:3em"class="btn-lg glyphicon glyphicon-education" aria-hidden="true"></span> 
								</div>
								<div class="col-xs-12 col-md-9" style="text-align:justify">
								Informasi akademik bersumber pada Database Sistem Informasi Akademik yang telah ditampung dan diolah dari datawarehouse UIN Sunan Ampel secara periodik.
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-5">
					<div class="panel panel-default ">
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12 col-md-3">
								  <span style="font-size:2em"class="btn-lg glyphicon glyphicon-search" aria-hidden="true"></span> 
								</div>
								<div class="col-xs-12 col-md-9" style="text-align:justify">
								Kriteria Data yang ditampilkan meliputi data Mahasiswa Baru, Mahasiswa Aktif, maupun Wisudawan jenjang Sarjana (S1)
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-5">
					<div class="panel panel-default ">
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12 col-md-3">
								  <span style="font-size:1.5em"class="btn-lg glyphicon glyphicon-calendar" aria-hidden="true"></span> 
								</div>
								<div class="col-xs-12 col-md-9" style="text-align:justify">
								Tanggal Diperbarui </br>02 Desember 2017
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
          </div>
          <div class="col-sm-10 col-lg-11 mx-auto" >
			<div class="row">
				<div class="col-xs-12 col-md-12">
					<div class="panel panel-default ">
						<div class="panel-body"style="font-size:1.3em;">
						Mahasiswa Baru
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-3">
					<div class="panel panel-default ">
						<div class="panel-heading text-center" >
						Daya Tampung Mahasiswa S1
						</div>
						<div class="panel-body">
							<div class="page-header" style="margin-top:-1.5em;">
							  <h1>7395 <small>Orang</small></h1>
							</div>
							<button class="btn btn-success" style="width:100%;" type="button">
							  Messages
							</button>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-3">
					<div class="panel panel-default ">
						<div class="panel-heading text-center" >
						Pendaftar Mahasiswa S1
						</div>
						<div class="panel-body">
							<div class="page-header" style="margin-top:-1.5em;">
							  <h1>7395 <small>Orang</small></h1>
							</div>
							<button class="btn btn-success" style="width:100%;" type="button">
							  Messages
							</button>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-3">
					<div class="panel panel-default ">
						<div class="panel-heading text-center " >
						Diterima Mahasiswa S1
						</div>
						<div class="panel-body">
							<div class="page-header" style="margin-top:-1.5em;">
							  <h1>7395 <small>Orang</small></h1>
							</div>
							<button class="btn btn-success" style="width:100%;" type="button">
							  Messages
							</button>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-3">
					<div class="panel panel-default ">
						<div class="panel-heading text-center" >
						Registrasi Mahasiswa S1
						</div>
						<div class="panel-body">
							<div class="page-header" style="margin-top:-1.5em;">
							  <h1>7395 <small>Orang</small></h1>
							</div>
							<button class="btn btn-success" style="width:100%;" type="button">
							  Messages
							</button>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-12">
					<div class="panel panel-default ">
					</div>
				</div>
				
			</div>
          </div>
          <div class="col-sm-10 col-lg-11 mx-auto" >
			<div class="row">
				<div class="col-xs-12 col-md-3">
					<div class="row">
						<div class="col-xs-12 col-md-12">
							<div class="panel panel-default ">
								<div class="panel-heading text-center" >
								Mahasiswa Asing (S1)
								</div>
								<div class="panel-body">
									<div class="page-header" style="margin-top:-1.5em;">
									  <h1>7395 <small>Orang</small></h1>
									</div>
									<button class="btn btn-warning" style="width:100%;" type="button">
									  Messages
									</button>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-md-12">
							<div class="panel panel-default ">
								<div class="panel-heading text-center" >
								Mahasiswa Pascasarjana 
								</div>
								<div class="panel-body">
									<div class="page-header" style="margin-top:-1.5em;">
									  <h1>7395 <small>Orang</small></h1>
									</div>
									<button class="btn btn-warning" style="width:100%;" type="button">
									  Messages
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-9">
					<div class="row">
						<div class="col-xs-12 col-md-12">
							<div class="panel panel-default ">
								<div class="panel-body">
								<p style="font-size:1.3em">Sebaran Mahasiswa Tahun 2017</p><hr>
									
								</div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
          </div>
        </div>
      </div>
    </section>
	
	<section class="content-section bg-light" id="services">
      <div class="container text-center">
        <div class="row">
          <div class="col-sm-10 col-lg-11 mx-auto">
            
          </div>
        </div>
      </div>
    </section>
	
	<section class="content-section bg-light" id="portfolio">
      <div class="container text-center">
        <div class="row">
          <div class="col-sm-10 col-lg-11 mx-auto">
            
          </div>
        </div>
      </div>
    </section>
	<section class="content-section bg-light" id="contact">
      <div class="container text-center">
        <div class="row">
          <div class="col-sm-10 col-lg-11 mx-auto">
            
          </div>
        </div>
      </div>
    </section>