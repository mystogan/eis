
	<script src="<?php echo base_url();?>assets/js/grafik/highmaps.js"></script>
	<script src="<?php echo base_url();?>assets/js/grafik/exporting.js"></script>
	<script src="<?php echo base_url();?>assets/js/grafik/id-all.js"></script>

	<style>
	#container2 {
    height: 500px;
    min-width: 310px;
    max-width: 800px;
    margin: 0 auto;
}
.loading {
    margin-top: 10em;
    text-align: center;
    color: gray;
}
	</style>
<div class="jumbotron text-center"style="padding-top:5em;">
<div class="container">
	  <img width="60em" src="<?php echo base_url();?>assets/images/uin.png"/>
	  <h1 style="color:#0c5401"><small style="color:#f0ad4e">KEMAHASISWAAN</small></h1>
	  <p style="color:#0c5401">Data Mahasiswa Baru, Mahasiswa Aktif, maupun Wisudawan jenjang Sarjana (S1)</p>
</div>
</div>

<div class="tahun text-right" style="background-color:#eee;padding-bottom:1em;">
<div class="container">
	  <h1 style="color:#0c5401">Tahun 2017 <small><span data-toggle="modal" data-target="#myModal" style="cursor:pointer" class="glyphicon glyphicon-cog"></span></small></h1>
</div>
</div>


<div class="container-fluid ">
<div class="text-center">
  </div>
  <div class="row">
    <div class="col-sm-6 text-right">
	<div class="panel panel-default  " style="border-color:#cacaca">
		<div class="panel-body">
			<p style="font-size:1.3em">Total Mahasiswa Per Fakultas</p><hr>
				<div class="row">
					<div class="col-md-6">
						<ul data-pie-id="svg3" style="text-align:right;">
							<?php foreach ($getAllMhsFakultas as $HgetAllMhsFakultas): ?>
								<li data-value="<?php echo $HgetAllMhsFakultas['hasil']; ?>"><?php echo $HgetAllMhsFakultas['namaunit']; ?> (<?php echo $HgetAllMhsFakultas['hasil']; ?>)</li>
							<?php endforeach; ?>
						</ul>
					</div>
					<div class="col-md-6">
						<div id="svg3"></div>
					</div>
				</div>
		</div>
	</div>
    </div>
    <div class="col-sm-6 text-right">
	<div class="panel panel-default  " style="border-color:#cacaca;height:25.5em">
		<div class="panel-body">
		<p style="font-size:1.3em">Total Mahasiswa Per Jenis Kelamin</p><hr>
			<div id="container1"></div>
		</div>
	</div>
    </div>
    <div class="col-sm-12 text-right">
	<div class="panel panel-default  " style="border-color:#cacaca">
		<div class="panel-body">
			<div id="container" style="min-width:300px; height:400px; margin:0 auto"></div>
		</div>
	</div>
    </div>
  </div>
</div>

<div class="container-fluid bg-grey">
  <div class="row">
    <div class="col-sm-4 text-right">
	<h1 style="color:#0c5401"> SEBARAN MAHASISWA TAHUN 2017</h1>
      <span class="glyphicon glyphicon-globe logo slideanim"></span>
    </div>
    <div class="col-sm-8">
	<div id="container2"></div>
    </div>
  </div>
</div>


	  <!-- Modal -->
<div class="modal fade " id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top:10em;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Pengaturan</h4>
      </div>
      <div class="modal-body text-left">
        <div class="form-group">
		  <label for="sel1">Pilih Tahun :</label>
		  <select class="form-control" id="sel1">
			<option>2017</option>
			<option>2016</option>
			<option>2015</option>
			<option>2014</option>
		  </select>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-warning">Pilih Tahun</button>
      </div>
    </div>
  </div>
</div>

	<script src="<?php echo base_url();?>assets/js/grafik/highcharts.js"></script>
	<script src="<?php echo base_url();?>assets/js/grafik/drilldown.js"></script>
	<script src="<?php echo base_url();?>assets/js/grafik/data.js"></script>
	<script src="<?php echo base_url();?>assets/js/grafik/proses.js"></script>
	<script src="<?php echo base_url();?>assets/js/grafik/snap.svg-min.js"></script>
<script>

// Prepare demo data
// Data is joined to map using value of 'hc-key' property by default.
// See API docs for 'joinBy' for more info on linking data and map.
var data = [
    ['id-3700', 0],
    ['id-ac', 1],
    ['id-ki', 2],
    ['id-jt', 3],
    ['id-be', 4],
    ['id-bt', 5],
    ['id-kb', 6],
    ['id-bb', 7],
    ['id-ba', 8],
    ['id-ji', 9],
    ['id-ks', 10],
    ['id-nt', 11],
    ['id-se', 12],
    ['id-kr', 13],
    ['id-ib', 14],
    ['id-su', 15],
    ['id-ri', 16],
    ['id-sw', 17],
    ['id-la', 18],
    ['id-sb', 19],
    ['id-ma', 20],
    ['id-nb', 21],
    ['id-sg', 22],
    ['id-st', 23],
    ['id-pa', 24],
    ['id-jr', 25],
    ['id-1024', 26],
    ['id-jk', 27],
    ['id-go', 28],
    ['id-yo', 29],
    ['id-kt', 30],
    ['id-sl', 31],
    ['id-sr', 32],
    ['id-ja', 33]
];

// Create the chart
Highcharts.mapChart('container2', {
    chart: {
        map: 'countries/id/id-all'
    },

    title: {
        text: 'Highmaps basic demo'
    },

    subtitle: {
        text: 'Source map: <a href="http://code.highcharts.com/mapdata/countries/id/id-all.js">Indonesia</a>'
    },

    mapNavigation: {
        enabled: true,
        buttonOptions: {
        verticalAlign: 'bottom'
        }
    },

    colorAxis: {
        min: 0
    },

    series: [{
        data: data,
        name: 'Random data',
        states: {
            hover: {
                color: '#BADA55'
            }
        },
        // dataLabels: {
            // enabled: true,
            // format: '{point.name}'
        // }
    }]
});

</script>

	<script type="text/javascript">

	Highcharts.chart('container1', {
			chart: {
					plotBackgroundColor: null,
					plotBorderWidth: 0,
					plotShadow: false
			},

			title: {
					text: '',
					align: 'center',
					verticalAlign: 'middle',
					y: 20
			},
			tooltip: {
					pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			},
			plotOptions: {
					pie: {
							dataLabels: {
									enabled: true,
									distance: -70,
									style: {
											fontWeight: 'bold',
											color: 'white'
									}
							},
							startAngle: -90,
							endAngle: 90,
							center: ['50%', '50%']
					}
			},
			series: [{
					type: 'pie',
					name: 'Browser share',
					innerSize: '40%',
					data: [
							['Perempuan',   <?php echo $getAllMhsJK[1]['count']; ?>],
							['Laki-Laki',       <?php echo $getAllMhsJK[0]['count']; ?>],
							{
									name: 'Proprietary or Undetectable',
									y: 0.2,
									dataLabels: {
											enabled: false
									}
							}
					]
			}]
	});
	</script>
