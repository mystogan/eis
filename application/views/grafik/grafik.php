
	<script type="text/javascript">
	function diagram(jk){
		if(jk == "Laki-Laki"){
			jk = 1;
		}else {
			jk = 2;
		}
		location.href="<?php echo base_url();?>index.php/admin/batang/"+jk;
	}
window.onload = function () {
	var chart = new CanvasJS.Chart("chartContainer",
	{
		theme: "theme3",
		title:{
			text: "as"
		},
		animationEnabled: true,
		animationDuration: 2000,
		legend: {
			verticalAlign: "bottom",
			horizontalAlign: "center"
		},

		data: [
		{
			click: function(e){
				//alert (e.dataPoint.indexas+e.dataPoint.y+e.dataPoint.indexLabel);
				//diagram(e.dataPoint.indexLabel);
			},
			indexLabelFontSize: 20,
			indexLabelFontFamily: "Monospace",
			indexLabelFontColor: "darkgrey",
			indexLabelLineColor: "darkgrey",
			indexLabelPlacement: "outside",			
			type: "pie",
			showInLegend: true,
			toolTipContent: "{y} Orang - #percent %",
			legendText: "{indexLabel}",
			dataPoints: [
				{  y: <?php print_r ($getAllDosen['hasil']);?>, indexLabel: "Dosen",indexas: "1" },
				{  y: <?php print_r ($getAllStaff['hasil']);?>, indexLabel: "Pegawai", indexas: "2"},	
				{  y: <?php print_r ($getAllMhs['hasil']);?>, indexLabel: "Mahasiswa", indexas: "3"},	
			]
		}
		]
	});
	chart.render();
}
	</script>
	<script src="<?php echo base_url();?>asset/js/grafik/canvasjs.min.js"></script>

<div id="wrapper">
	<div id="wrapper_menu">
		<div id="judulnya">Grafik</div>
		<div id="scrollnya">
		
		<div style="height:100%;"id="wrapper_submenu">
			<div id="juduldalem">
				<div id="chartContainer" style="padding-top:2vw;height: 400px; width: 95%;"></div>
				<div id="jumlaha"></div>
			</div>
			
		</div>
		</div>
	</div>
</div>
