
	<link rel="stylesheet" href="<?php echo base_url();?>asset/css/bootstrap.min.css"  type="text/css"/>
	<!-- DataTables CSS -->
	<link href="<?php echo base_url();?>asset/css/vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
	<!-- DataTables Responsive CSS -->
	<link href="<?php echo base_url();?>asset/css/vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">


	<!-- Custom Theme JavaScript -->
	<script src="<?php echo base_url();?>asset/css/vendor/datatables/js/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url();?>asset/css/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
	<script src="<?php echo base_url();?>asset/css/vendor/datatables-responsive/dataTables.responsive.js"></script>
	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
	<script>
	$(document).ready(function() {
		<?php echo $simpan ; $this->session->unset_userdata(array('hasil' => ''));?>
		$('#dataTables-example').DataTable({
				responsive: true
		});
	});
	</script>
	<div id="wrapper">
		<div id="wrapper_menu">
			<div id="judulnya">Grafik</div>
			<div id="scrollnya">
			
			<div style="height:100%;"id="wrapper_submenu">
				<div id="juduldalem">
					<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
							<tr>
								<th style="text-align:center;">NIM</th>
								<th style="text-align:center;">Nama Mahasiswa</th>
								<th style="text-align:center;">Program Studi</th>
							</tr>
						</thead>
						<tbody>

						<?php
						foreach($tabel as $hasil){ ?>
							<tr class="odd gradeX">
								<td><?php print_r ($hasil['nim']); ?></td>
								<td><?php print_r ($hasil['nama']); ?></td>
								<td><?php print_r ($hasil['prodi']); ?></td>
							</tr>
						<?php
						}
						?>
						
						</tbody>
					</table>

				</div>
					<div id="jumlaha">
						<a class="btn btn-default submit" href="<?php echo base_url();?>index.php/admin/batang/<?php echo $kel;?>">Kembali</a>
					</div>
			
			</div>
			</div>
		</div>
	</div>