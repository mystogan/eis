
<div class="jumbotron text-center"style="padding-top:5em;">
<div class="container">
	  <img width="60em" src="<?php echo base_url();?>assets/images/uin.png"/>
	  <h1 style="color:#0c5401"><small style="color:#f0ad4e">AKADEMIK</small></h1>
	  <p style="color:#0c5401">Data Mahasiswa Baru, Mahasiswa Aktif, maupun Wisudawan jenjang Sarjana (S1)</p>
</div>
</div>

<div class="tahun text-right" style="background-color:#eee;padding-bottom:1em;">
<div class="container">
	  <h1 style="color:#0c5401">Tahun 2017 <small ><span data-toggle="modal" data-target="#myModal" style="cursor:pointer" class="glyphicon glyphicon-cog"></span></small></h1>

</div>
</div>

<!-- Container (Services Section) -->
<div id="services" class="container-fluid text-center">
<div class="text-left">
    <h2><span style="color:#f0ad4e;" class="glyphicon glyphicon-user"></span>  Mahasiswa Baru</h2>
	</br>
  </div>
  <div class="row slideanim">
    <div class="col-sm-3">
		<div class="panel panel-default">
		  <div class="panel-heading">Daya Tampung Mahasiswa S1</div>
		  <div class="panel-body">
			<h1><?php echo $getAllMhsBaru['hasil']; ?> <small>Orang</small></h1>
			<hr>
				<button class="btn btn-success" style="width:100%;" type="button">
					Detil
				</button>
		  </div>
		</div>
    </div>
    <div class="col-sm-3">
		<div class="panel panel-default">
		  <div class="panel-heading">Pendaftar Mahasiswa S1</div>
		  <div class="panel-body">
			<!-- <h1>7395 <small>Orang</small></h1> -->
			<h3>Belum Ada Data di Database</h3>
			<hr>
				<button class="btn btn-success" style="width:100%;" type="button">
					Detil
				</button>
		  </div>
		</div>
    </div>
    <div class="col-sm-3">
		<div class="panel panel-default">
		  <div class="panel-heading">Diterima Mahasiswa S1</div>
		  <div class="panel-body">
				<h3>Belum Ada Data di Database</h3>
			<!-- <h1>7395 <small>Orang</small></h1> -->
			<hr>
				<button class="btn btn-success" style="width:100%;" type="button">
					Detil
				</button>
		  </div>
		</div>
    </div>
    <div class="col-sm-3">
		<div class="panel panel-default">
		  <div class="panel-heading">Registrasi Mahasiswa S1</div>
		  <div class="panel-body">
				<h3>Belum Ada Data di Database</h3>
			<!-- <h1>7395 <small>Orang</small></h1> -->
			<hr>
				<button class="btn btn-success" style="width:100%;" type="button">
					Detil
				</button>
		  </div>
		</div>
    </div>
  </div>
  <div class="text-left">
    <h2><span style="color:#f0ad4e;" class="glyphicon glyphicon-user"></span>  Mahasiswa Aktif</h2>
	</br>
  </div>
  <div class="row slideanim">
    <div class="col-sm-4">
		<div class="panel panel-default">
		  <div class="panel-heading">Mahasiswa Sarjana</div>
		  <div class="panel-body">
			<h1><?php echo $getAllMhs['hasil']; ?> <small>Orang</small></h1>
			<hr>
				<button class="btn btn-success" style="width:100%;" type="button">
					Detil
				</button>
		  </div>
		</div>
    </div>
    <div class="col-sm-4">
		<div class="panel panel-default">
		  <div class="panel-heading">Mahasiswa Pasca Sarjana</div>
		  <div class="panel-body">
			<h1><?php echo $getAllMhsPasca['hasil']; ?> <small>Orang</small></h1>
			<hr>
				<button class="btn btn-success" style="width:100%;" type="button">
					Detil
				</button>
		  </div>
		</div>
    </div>
    <div class="col-sm-4">
		<div class="panel panel-default">
		  <div class="panel-heading">Mahasiswa Asing (Sarjana)</div>
		  <div class="panel-body">
			<h1><?php echo $getAllMhsAsing['hasil']; ?> <small>Orang</small></h1>
			<hr>
				<button class="btn btn-success" style="width:100%;" type="button">
					Detil
				</button>
		  </div>
		</div>
    </div>
  </div>
</div>

	  <!-- Modal -->
<div class="modal fade " id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top:10em;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Pengaturan</h4>
      </div>
      <div class="modal-body text-left">
        <div class="form-group">
		  <label for="sel1">Pilih Tahun :</label>
		  <select class="form-control" id="sel1">
			<option>2017</option>
			<option>2016</option>
			<option>2015</option>
			<option>2014</option>
		  </select>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-warning">Pilih Tahun</button>
      </div>
    </div>
  </div>
</div>
