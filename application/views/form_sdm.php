
<div class="jumbotron text-center"style="padding-top:5em;">
<div class="container">
	  <img width="60em" src="<?php echo base_url();?>assets/images/uin.png"/>
	  <h1 style="color:#0c5401"><small style="color:#f0ad4e">SUMBER DAYA MANUSIA</small></h1>
	  <p style="color:#0c5401">Data Mahasiswa Baru, Mahasiswa Aktif, maupun Wisudawan jenjang Sarjana (S1)</p>
</div>
</div>

<div class="tahun text-right" style="background-color:#eee;padding-bottom:1em;">
<div class="container">
	  <h1 style="color:#0c5401">Tahun 2017 <small><span data-toggle="modal" data-target="#myModal" style="cursor:pointer" class="glyphicon glyphicon-cog"></span></small></h1>
</div>
</div>

<!-- Container (Services Section) -->
<div id="services" class="container-fluid text-center">
<div class="text-left">
	<h2><span style="color:#f0ad4e;" class="glyphicon glyphicon-user"></span> Sumber Daya</h2>
	</br>
</div>
  <div class="row slideanim">
    <div class="col-sm-4">
		<div class="panel panel-default">
		  <div class="panel-heading">PEGAWAI</div>
		  <div class="panel-body">
			<h1><?php echo $getAllStaff['hasil']; ?> <small>Orang</small></h1>
			<hr>
				<button class="btn btn-success" style="width:100%;" type="button">
					Detil
				</button>
		  </div>
		</div>
    </div>
    <div class="col-sm-4">
		<div class="panel panel-default">
		  <div class="panel-heading">DOSEN</div>
		  <div class="panel-body">
			<h1><?php echo $getAllDosen['hasil']; ?> <small>Orang</small></h1>
			<hr>
				<button class="btn btn-success" style="width:100%;" type="button">
					Detil
				</button>
		  </div>
		</div>
    </div>
    <div class="col-sm-4">
		<div class="panel panel-default">
		  <div class="panel-heading">GURU BESAR</div>
		  <div class="panel-body">
			<h1><?php echo $getAllGuruBesar['hasil']; ?> <small>Orang</small></h1>
			<hr>
				<button class="btn btn-success" style="width:100%;" type="button">
					Detil
				</button>
		  </div>
		</div>
    </div>
  </div>
<div class="text-left">
	<h2><span style="color:#f0ad4e;" class="glyphicon glyphicon-user"></span> Lulusan</h2>
	</br>
</div>
  <div class="row slideanim">
    <div class="col-sm-4">
		<div class="panel panel-default">
		  <div class="panel-heading">Tingkat Pendidikan Sarjana</div>
		  <div class="panel-body">
			<h1><?php echo $getAllTotalLulusanS1['hasil']; ?> <small>Orang</small></h1>
			<hr>
				<button class="btn btn-success" style="width:100%;" type="button">
					Detil
				</button>
		  </div>
		</div>
    </div>
    <div class="col-sm-4">
		<div class="panel panel-default">
		  <div class="panel-heading">Tingkat Pendidikan Magister</div>
		  <div class="panel-body">
			<h1><?php echo $getAllTotalLulusanS2['hasil']; ?> <small>Orang</small></h1>
			<hr>
				<button class="btn btn-success" style="width:100%;" type="button">
					Detil
				</button>
		  </div>
		</div>
    </div>
    <div class="col-sm-4">
		<div class="panel panel-default">
		  <div class="panel-heading">Tingkat Pendidikan Doktor</div>
		  <div class="panel-body">
			<h1><?php echo $getAllTotalLulusanS3['hasil']; ?> <small>Orang</small></h1>
			<hr>
				<button class="btn btn-success" style="width:100%;" type="button">
					Detil
				</button>
		  </div>
		</div>
    </div>
  </div>
</div>

<div class="container-fluid ">
<div class="text-center">
	<hr style="border-color:#cacaca;">
    <h2>Dosen</h2>
	<hr style="border-color:#cacaca;">
  </div>
  <div class="row">
    <div class="col-sm-6 text-right">
	<div class="panel panel-default  " style="border-color:#cacaca">
	<div class="panel-body">
			<p style="font-size:1.3em">Lulusan Dosen</p><hr>
				<div class="row">
					<div class="col-md-6">
						<ul data-pie-id="svg4" style="text-align:right;">
							<li data-value="<?php echo $getDosenLulusanS1['hasil']; ?>">Sarjana (<?php echo $getDosenLulusanS1['hasil']; ?>)</li>
							<li data-value="<?php echo $getDosenLulusanS2['hasil']; ?>">Magister (<?php echo $getDosenLulusanS2['hasil']; ?>)</li>
							<li data-value="<?php echo $getDosenLulusanS3['hasil']; ?>">Doktor (<?php echo $getDosenLulusanS3['hasil']; ?>)</li>
						</ul>
					</div>
					<div class="col-md-6">
						<div id="svg4"></div>
					</div>
				</div>
		</div>
	</div>
    </div>
    <div class="col-sm-6">
	<div class="panel panel-default " style="border-color:#cacaca">
		<div class="panel-body">
			<p style="font-size:1.3em">Pangkat Dosen</p><hr>
				<div class="row">
					<div class="col-md-6">
						<ul data-pie-id="svg1" style="text-align:right;">
							<?php foreach ($getPangkatDosen as $HgetPangkatDosen): ?>
								<li data-value="<?php echo $HgetPangkatDosen['hasil']; ?>"><?php echo $HgetPangkatDosen['nama_pangkat']; ?> (<?php echo $HgetPangkatDosen['hasil']; ?>)</li>
							<?php endforeach; ?>
						</ul>
					</div>
					<div class="col-md-6">
						<div id="svg1"></div>
					</div>
				</div>
		</div>
	</div>
    </div>
    <div class="col-sm-12">
	<div class="panel panel-default " style="border-color:#cacaca">
		<div class="panel-body">
			<p style="font-size:1.3em">Jabatan Fungsional</p><hr>
				<div class="row">
					<div class="col-md-6">
						<ul data-pie-id="svg" style="text-align:right;">
							<?php foreach ($getJafungDosen as $HgetJafungDosen): ?>
								<li data-value="<?php echo $HgetJafungDosen['hasil']; ?>"><?php echo $HgetJafungDosen['nama_jafung']; ?> (<?php echo $HgetJafungDosen['hasil']; ?>)</li>
							<?php endforeach; ?>
						</ul>
					</div>
					<div class="col-md-6">
						<div id="svg"></div>
					</div>
				</div>
		</div>
	</div>
    </div>
  </div>
</div>
<div class="container-fluid ">
<div class="text-center">
	<hr style="border-color:#cacaca;">
    <h2>Pegawai</h2>
	<hr style="border-color:#cacaca;">
  </div>
  <div class="row">
    <div class="col-sm-6 text-right">
	<div class="panel panel-default  " style="border-color:#cacaca">
		<div class="panel-body">
			<p style="font-size:1.3em">Lulusan Pegawai</p><hr>
				<div class="row">
					<div class="col-md-6">
						<ul data-pie-id="svg5" style="text-align:right;">
							<li data-value="<?php echo $getPegawaiLulusanS1['hasil']; ?>">Sarjana (<?php echo $getPegawaiLulusanS1['hasil']; ?>)</li>
							<li data-value="<?php echo $getPegawaiLulusanS2['hasil']; ?>">Magister (<?php echo $getPegawaiLulusanS2['hasil']; ?>)</li>
							<li data-value="<?php echo $getPegawaiLulusanS3['hasil']; ?>">Doktor (<?php echo $getPegawaiLulusanS3['hasil']; ?>)</li>
						</ul>
					</div>
					<div class="col-md-6">
						<div id="svg5"></div>
					</div>
				</div>
		</div>
	</div>
    </div>
    <div class="col-sm-6">
	<div class="panel panel-default " style="border-color:#cacaca">
		<div class="panel-body">
			<p style="font-size:1.3em">Pangkat Pegawai</p><hr>
				<div class="row">
					<div class="col-md-6">
						<ul data-pie-id="svg3" style="text-align:right;">
							<?php foreach ($getPangkatDosen as $HgetPangkatDosen): ?>
								<li data-value="<?php echo $HgetPangkatDosen['hasil']; ?>"><?php echo $HgetPangkatDosen['nama_pangkat']; ?> (<?php echo $HgetPangkatDosen['hasil']; ?>)</li>
							<?php endforeach; ?>
						</ul>
					</div>
					<div class="col-md-6">
						<div id="svg3"></div>
					</div>
				</div>
		</div>
	</div>
    </div>
	<div class="col-sm-12">
	<div class="panel panel-default " style="border-color:#cacaca">
	<!-- <div class="panel-body">
			<p style="font-size:1.3em">Jabatan Fungsional</p><hr>
				<div class="row">
					<div class="col-md-6">
						<ul data-pie-id="svg2" style="text-align:right;">
							<li data-value="60">Adab dan Humaniora (60)</li>
							<li data-value="20">Dakwah dan Komunikasi (20)</li>
							<li data-value="12">Tarbiyah dan Keguruan (12)</li>
							<li data-value="32">Ushuludin dan Filsafat (32)</li>
							<li data-value="50">Syariah dan Hukum (50)</li>
							<li data-value="50">Sains an Teknologi (50)</li>
							<li data-value="50">Ekonomi dan Bisnis Islam (50)</li>
							<li data-value="50">Psikologi dan Kesehatan (30)</li>
							<li data-value="50">Ilmu Sosial dan Ilmu Politik (50)</li>
						</ul>
					</div>
					<div class="col-md-6">
						<div id="svg2"></div>
					</div>
				</div>
		</div> -->
	</div>
    </div>
  </div>
</div>

	  <!-- Modal -->
<div class="modal fade " id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top:10em;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Pengaturan</h4>
      </div>
      <div class="modal-body text-left">
        <div class="form-group">
		  <label for="sel1">Pilih Tahun :</label>
		  <select class="form-control" id="sel1">
			<option>2017</option>
			<option>2016</option>
			<option>2015</option>
			<option>2014</option>
		  </select>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-warning">Pilih Tahun</button>
      </div>
    </div>
  </div>
</div>

	<script src="<?php echo base_url();?>assets/js/grafik/proses.js"></script>
	<script src="<?php echo base_url();?>assets/js/grafik/snap.svg-min.js"></script>
