
  	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/reset.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/form_akademik.css" type="text/css "/>
	<style>
	 .input-group-addon {
    padding: 6px 12px;
    font-size: 14px;
    font-weight: 400;
    line-height: 1;
    color: #fff;
    text-align: center;
    background-color: #f0af54;
    border: 1px solid #ccc;
    border-radius: 4px;
}
	</style>
	
<div class="jumbotron text-center"style="padding-top:6em;">
<div class="container">
	  <img width="60em" src="<?php echo base_url();?>assets/images/uin.png"/>
	  <h1 style="color:#f0ad4e">LOGIN</h1>
	  <p style="color:#0c5401">Selamat Datang di <strong>Executive Information System</strong> UINSA</p>
</div>
</div>

	<div class="container">
  <div class="row">
    <div class="Absolute-Center is-Responsive">
      <div id="logo-container"></div>
      <div class="col-sm-12 col-md-10 col-md-offset-1">
        <form action="" id="loginForm">
          <div class="form-group input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
            <input class="form-control" type="text" name='username' placeholder="username"/>          
          </div>
          <div class="form-group input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
            <input class="form-control" type="password" name='password' placeholder="password"/>     
          </div>
          <div class="form-group">
            <button type="button" class="btn btn-success btn-block">Login</button>
          </div>
        </form>        
      </div>  
    </div>    
  </div>
</div>