<?php
class Pasca_model extends CI_Model {
	function __construct(){
		parent::__construct();
		 $this->db3 = $this->load->database('Pasca', TRUE);
	}
	function login($username,$password){
		$this->db->select('*');
		$this->db->from("login");
		$this->db->where('username =', $username);
		$this->db->where('password =', $password);
		$data = $this->db->count_all_results();
		return $data;
	}

	function getAllMhsPasca(){
		$query = $this->db3->query("select count(a.nama) as hasil
																from akademik.ms_mahasiswa a
																join gate.ms_unit b on b.kodeunit = a.kodeunit
																where statusmhs = 'A'
																and b.kodeunitparent = 'PSC'");
		$data = $query->result_array();
		return $data[0];
	}

}
?>
