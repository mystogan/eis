<?php
class Home_model extends CI_Model {
	function __construct(){
		parent::__construct();

	}
	function coba(){
		$this->db->select('*');
		$this->db->from('skpi.skpi_bahasa');
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getAllMhs(){
		$this->db->select('count(*) as hasil');
		$this->db->from('akademik.ms_mahasiswa');
		$this->db->where("statusmhs = 'A'");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data[0];
	}
	function getAllMhsFakultas(){
		// $this->db->select('count(a.nama) as hasil,c.namaunita',false);
		// $this->db->from('akademik.ms_mahasiswa a');
		// $this->db->join('gate.ms_unit b', 'a.kodeunit = b.kodeunit');
		// $this->db->join('gate.ms_unit c', 'c.kodeunit = b.kodeunit');
		// $this->db->where("statusmhs = 'A'");
		// $this->db->group_by("c.kodeunitparent");
		// $this->db->group_by("c.namaunit");
		// $query = $this->db->get();
		$query = $this->db->query("select count(nama) as hasil,c.namaunit
															from akademik.ms_mahasiswa a
															join gate.ms_unit b on a.kodeunit = b.kodeunit
															join gate.ms_unit c on c.kodeunit = b.kodeunitparent
															where a.statusmhs = 'A'
															group by c.kodeunitparent,c.namaunit");
		$data = $query->result_array();
		return $data;
	}
	function getAllMhsJK(){
		$query = $this->db->query("select sex,count(sex)
																from akademik.ms_mahasiswa
																where statusmhs = 'A'
																and sex is not null
																group by sex");
		$data = $query->result_array();
		return $data;
	}
	function getAllMhsPerTahun($tahun){
		$tahun  = $tahun - 5;
		$qur = "SELECT SUBSTRING(periodemasuk,1,4) as tahun ,count(nama)
						FROM akademik.ms_mahasiswa
						where statusmhs = 'A'
						and substr(periodemasuk,0,5)::text > '$tahun'
						group by  SUBSTRING(periodemasuk,1,4)
						order by 1";
		$query = $this->db->query($qur);
		$data = $query->result_array();
		return $data;
	}
	function getAllMhsBaru($tahun){
		$query = $this->db->query("select count(nama) as hasil
															from akademik.ms_mahasiswa
															where statusmhs = 'A'
															and substr(periodemasuk,0,5)::text = '$tahun'");
		$data = $query->result_array();
		return $data[0];
	}
	function getAllMhsAsing(){
		$query = $this->db->query("select count(nama) as hasil
																from akademik.ms_mahasiswa
																where kodewn = 'WNA'
																and statusmhs = 'A'");
		$data = $query->result_array();
		return $data[0];
	}

}
?>
