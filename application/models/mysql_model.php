<?php
class Mysql_model extends CI_Model {
	function __construct(){
		parent::__construct();
		 $this->db2 = $this->load->database('mysqle', TRUE);
	}
	function login($username = "0",$password = '0'){
		$this->db->select('*');
		$this->db->from("tbsimpeguser");
		$this->db->where('username =', $username);
		$this->db->where('password =', $password);
		$this->db->where('id_usergroup =', '20');
		$data = $this->db->count_all_results();
		return $data;
	}
	function coba2(){
		$this->db2->select('*');
		$this->db2->from('m_pangkat');
		$query = $this->db2->get();
		$data = $query->result_array();
		return $data;
	}
	function getAllStaff(){
		$this->db2->select('count(nama) as hasil');
		$this->db2->from('tbpegawai p');
		$this->db2->join('m_unit u', 'p.id_unitkerja=u.id ');
		$this->db2->where('id_tipe != 2');
		$this->db2->where('id_status_aktif = 1');
		$this->db2->order_by("p.nama", "asc");
		$query = $this->db2->get();
		$data = $query->result_array();
		return $data[0];
	}

	function getAllDosen(){
		$this->db2->select('count(nama) as hasil');
		$this->db2->from('tbpegawai p');
		$this->db2->join('m_unit u', 'p.id_unitkerja=u.id ');
		$this->db2->where('id_tipe = 2');
		$this->db2->where('id_status_aktif = 1');
		$this->db2->order_by("p.nama", "asc");
		$query = $this->db2->get();
		$data = $query->result_array();
		return $data[0];
	}
	function getAllGuruBesar(){
		$this->db2->select('COUNT(nip) as hasil');
		$this->db2->from('tbpegawai ');
		$this->db2->where('id_jafung = 1');
		$this->db2->where('id_status_aktif = 1');
		$query = $this->db2->get();
		$data = $query->result_array();
		return $data[0];
	}
	function getAllTotalLulusanS1(){
		$this->db2->select('COUNT(nip) as hasil');
		$this->db2->from('tbpegawai a');
		$this->db2->join('tbriwpendidikan b', 'a.id = b.id_pegawai ');
		$this->db2->join('m_pendidikan c', 'c.id = b.tingkat');
		$this->db2->where('c.kode_pendidikan = 10');
		$this->db2->where('id_status_aktif = 1');
		$query = $this->db2->get();
		$data = $query->result_array();
		return $data[0];
	}
	function getDosenLulusanS1(){
		$this->db2->select('COUNT(nip) as hasil');
		$this->db2->from('tbpegawai a');
		$this->db2->join('tbriwpendidikan b', 'a.id = b.id_pegawai ');
		$this->db2->join('m_pendidikan c', 'c.id = b.tingkat');
		$this->db2->where('c.kode_pendidikan = 10');
		$this->db2->where('id_tipe = 2');
		$this->db2->where('id_status_aktif = 1');
		$query = $this->db2->get();
		$data = $query->result_array();
		return $data[0];
	}
	function getPegawaiLulusanS1(){
		$this->db2->select('COUNT(nip) as hasil');
		$this->db2->from('tbpegawai a');
		$this->db2->join('tbriwpendidikan b', 'a.id = b.id_pegawai ');
		$this->db2->join('m_pendidikan c', 'c.id = b.tingkat');
		$this->db2->where('c.kode_pendidikan = 10');
		$this->db2->where('id_tipe = 1');
		$this->db2->where('id_status_aktif = 1');
		$query = $this->db2->get();
		$data = $query->result_array();
		return $data[0];
	}
	function getAllTotalLulusanS2(){
		$this->db2->select('COUNT(nip) as hasil');
		$this->db2->from('tbpegawai a');
		$this->db2->join('tbriwpendidikan b', 'a.id = b.id_pegawai ');
		$this->db2->join('m_pendidikan c', 'c.id = b.tingkat');
		$this->db2->where('c.kode_pendidikan = 11');
		$this->db2->where('id_status_aktif = 1');
		$query = $this->db2->get();
		$data = $query->result_array();
		return $data[0];
	}
	function getDosenLulusanS2(){
		$this->db2->select('COUNT(nip) as hasil');
		$this->db2->from('tbpegawai a');
		$this->db2->join('tbriwpendidikan b', 'a.id = b.id_pegawai ');
		$this->db2->join('m_pendidikan c', 'c.id = b.tingkat');
		$this->db2->where('c.kode_pendidikan = 11');
		$this->db2->where('id_tipe = 2');
		$this->db2->where('id_status_aktif = 1');
		$query = $this->db2->get();
		$data = $query->result_array();
		return $data[0];
	}
	function getPegawaiLulusanS2(){
		$this->db2->select('COUNT(nip) as hasil');
		$this->db2->from('tbpegawai a');
		$this->db2->join('tbriwpendidikan b', 'a.id = b.id_pegawai ');
		$this->db2->join('m_pendidikan c', 'c.id = b.tingkat');
		$this->db2->where('c.kode_pendidikan = 11');
		$this->db2->where('id_tipe = 1');
		$this->db2->where('id_status_aktif = 1');
		$query = $this->db2->get();
		$data = $query->result_array();
		return $data[0];
	}
	function getAllTotalLulusanS3(){
		$this->db2->select('COUNT(nip) as hasil');
		$this->db2->from('tbpegawai a');
		$this->db2->join('tbriwpendidikan b', 'a.id = b.id_pegawai ');
		$this->db2->join('m_pendidikan c', 'c.id = b.tingkat');
		$this->db2->where('c.kode_pendidikan = 12');
		$this->db2->where('id_status_aktif = 1');
		$query = $this->db2->get();
		$data = $query->result_array();
		return $data[0];
	}
	function getDosenLulusanS3(){
		$this->db2->select('COUNT(nip) as hasil');
		$this->db2->from('tbpegawai a');
		$this->db2->join('tbriwpendidikan b', 'a.id = b.id_pegawai ');
		$this->db2->join('m_pendidikan c', 'c.id = b.tingkat');
		$this->db2->where('c.kode_pendidikan = 12');
		$this->db2->where('id_tipe = 2');
		$this->db2->where('id_status_aktif = 1');
		$query = $this->db2->get();
		$data = $query->result_array();
		return $data[0];
	}
	function getPegawaiLulusanS3(){
		$this->db2->select('COUNT(nip) as hasil');
		$this->db2->from('tbpegawai a');
		$this->db2->join('tbriwpendidikan b', 'a.id = b.id_pegawai ');
		$this->db2->join('m_pendidikan c', 'c.id = b.tingkat');
		$this->db2->where('c.kode_pendidikan = 12');
		$this->db2->where('id_tipe = 1');
		$this->db2->where('id_status_aktif = 1');
		$query = $this->db2->get();
		$data = $query->result_array();
		return $data[0];
	}
	function getPangkatDosen(){
		$this->db2->select('COUNT(nip) as hasil,nama_pangkat');
		$this->db2->from('tbpegawai a');
		$this->db2->join('m_pangkat b', 'a.id_pangkat = b.id');
		$this->db2->where('id_tipe = 2');
		$this->db2->where('id_status_aktif = 1');
		$this->db2->group_by("b.id");
		$query = $this->db2->get();
		$data = $query->result_array();
		return $data;
	}
	function getPangkatPegawai(){
		$this->db2->select('COUNT(nip) as hasil,nama_pangkat');
		$this->db2->from('tbpegawai a');
		$this->db2->join('m_pangkat b', 'a.id_pangkat = b.id');
		$this->db2->where('id_tipe = 1');
		$this->db2->where('id_status_aktif = 1');
		$this->db2->group_by("b.id");
		$query = $this->db2->get();
		$data = $query->result_array();
		return $data;
	}
	function getJafungDosen(){
		$this->db2->select('COUNT(nip) as hasil,nama_jafung');
		$this->db2->from('tbpegawai a');
		$this->db2->join('m_jafung b', ' a.id_jafung = b.id');
		$this->db2->where('id_tipe = 2');
		$this->db2->where('id_status_aktif = 1');
		$this->db2->group_by("b.id");
		$query = $this->db2->get();
		$data = $query->result_array();
		return $data;
	}










	function getPortofolio($id = 0,$limit = 0){
		$this->db->select('*');
		$this->db->from('portofolio');
		$this->db->where('status = 1');
		if($id != 0){
			$this->db->where("id = $id");
		}
		if($limit != 0){
			$this->db->limit(10);
		}
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getSlider(){
		$this->db->select('*');
		$this->db->from('slider');
		$this->db->where("status = 1");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getlistSIngle($tabel){
		$this->db->select('*');
		$this->db->from($tabel);
		$this->db->order_by("id", "desc");
		$this->db->limit(5);
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getSIngle($tabel = 1,$id = 1){
		if($tabel == 1){
			$tabel = "proyek";
		}else if($tabel == 2){
			$tabel = "portofolio";
		}else {
			$tabel = "news";
		}
		$this->db->select('*');
		$this->db->from($tabel);
		$this->db->where("id = $id");
		$this->db->order_by("id", "desc");
		$this->db->limit(5);
		$query = $this->db->get();
		$data = $query->result_array();
		return $data[0];
	}
	function getTentang(){
		$this->db->select('*');
		$this->db->from('slide');
		$this->db->where("id = 3");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data[0];
	}
	function getProyek($id = 0){
		$this->db->select('*');
		$this->db->from('proyek');
		$this->db->where('status = 1');
		if($id != 0){
			$this->db->where("id = $id");
		}
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function save($table,$data){
		$this->db->insert($table, $data);
	}
	function update($id,$tabel,$data){
		$this->db->where('id', $id);
		$this->db->update($tabel, $data);
	}
	function update_password($user,$tabel,$data){
		$this->db->where('username', $user);
		$this->db->update($tabel, $data);
	}


}
?>
